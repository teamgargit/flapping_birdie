﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TouchJump : MonoBehaviour {

    //variables to show when the mouse/touch is used. pretty much a debug thing. probably wont make it to "final release"
    public Text m_TouchedUI;
    private int m_TouchedTimes;

    //public variable to fiddle with the up force in the unity editor. not likely to remain public in final release.
    public float m_UpForce;

    private bool m_UpwardsTrigger;

    //The start function. I deleted the start fnuction and then added it back in, and now cant remember what the original comment says. Something along the lines of "Start is called the first time a script is run" or some shit.
    void Start ()
    {
        //setting up the touch/click debug stuff.
        m_TouchedUI.text = "";
        m_TouchedTimes = 0;
        m_UpwardsTrigger = false;
    }
	// Update is called once per frame
	void Update () {

        //if the mouse button is pressed 
        if (Input.GetButton("Fire1") || Input.touchCount > 0)
        {
            if (!m_UpwardsTrigger)
            {
                //Debug.Log("Mouse Pressed");

                //stop the downward momentum
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                //add upward force
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, m_UpForce));

                //debuggy stuff
                m_TouchedTimes++;
                m_TouchedUI.text = "MousePress" + m_TouchedTimes;

                //set trigger so that only one lot of upward force is applied per click/touch.
                m_UpwardsTrigger = true;
            }
        }
        else
        {
            //the mouse/touch has been released, reset the trigger for the next touch/click event.
            m_UpwardsTrigger = false;
        }
    }
}
