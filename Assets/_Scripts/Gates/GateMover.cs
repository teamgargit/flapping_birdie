﻿using UnityEngine;
using System.Collections;
using System;

public class GateMover : MonoBehaviour {

    //sets the movement speed of the gate towards the right
    public float m_GateMovementSpeed;

    //sets variables for the gate spawning.
    public float m_GateSpawnTime;
    private float m_TimeToGateSpawn;
    public GameObject m_GateToSpawn;

	// Use this for initialization
	void Start ()
    {
        m_TimeToGateSpawn = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        MoveGates();
        SpawnGates();
        
	}

    private void SpawnGates()
    {
        //if the time to spawn is less than zero, spawn new gates
        if (m_TimeToGateSpawn <= 0f)
        {

            float randomSpawnLocation = (UnityEngine.Random.value * 9f) - 4.5f;
            Vector3 bottomSpawnLoc = new Vector3(10, randomSpawnLocation, 0);
            Vector3 topSpawnLoc = new Vector3(10, (randomSpawnLocation + 19), 0);
            Instantiate(m_GateToSpawn, bottomSpawnLoc, Quaternion.identity);
            Instantiate(m_GateToSpawn, topSpawnLoc, Quaternion.identity);
            m_TimeToGateSpawn = m_GateSpawnTime;
        }
        //reduce time to spawn by deltatime

        m_TimeToGateSpawn -= Time.deltaTime;
    }

    private void MoveGates()
    {
        //Find the gates
        GameObject[] gates = GameObject.FindGameObjectsWithTag("Gate");

        //move the gates
        foreach (GameObject thisGate in gates)
        {
            //float gateX = thisGate.transform.position.x;
            float gateX = (m_GateMovementSpeed * -1) * Time.deltaTime;
            thisGate.transform.position = thisGate.transform.position + new Vector3(gateX, 0, 0);
        }

        foreach (GameObject thisGate in gates)
        {
            if (thisGate.transform.position.x <= -1f)
            {
                Destroy(thisGate);
            }
        }
    }
}
